package com.cqcias.springboot.evaluacion.repository;

import java.util.List;import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cqcias.springboot.evaluacion.model.Persona;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long>{
	
	List<Persona> findByEstatus(String Estatus);

}