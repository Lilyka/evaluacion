package com.cqcias.springboot.evaluacion.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cqcias.springboot.evaluacion.exception.ResourceNotFoundException;
import com.cqcias.springboot.evaluacion.model.Persona;
import com.cqcias.springboot.evaluacion.repository.PersonaRepository;

@RestController
@RequestMapping("/")
public class PersonaController {
    @Autowired
    private PersonaRepository personaRepository;

    @GetMapping("personas")
    public List<Persona> getAllPersonas() {
    	return personaRepository.findAll();
    }

    @GetMapping("personas-activas")
    public List<Persona> getAllPersonasActivas(){        
        return personaRepository.findByEstatus("1");
    }
       
    @GetMapping("/persona/{id}")
    public ResponseEntity<Persona> getPersonaById(@PathVariable(value = "id") Long personaId)
        throws ResourceNotFoundException {
        Persona persona = personaRepository.findById(personaId)
          .orElseThrow(() -> new ResourceNotFoundException("Persona no encontrada " + personaId));
        return ResponseEntity.ok().body(persona);
    }
         
    @PostMapping("/personas")
    public Persona createPersona(@Valid @RequestBody Persona persona) {
        return personaRepository.save(persona);
    }

    @PutMapping("/persona/{id}")
    public ResponseEntity<Persona> updatePersona(@PathVariable(value = "id") Long personaId,
         @Valid @RequestBody Persona personaDetails) throws ResourceNotFoundException {
        Persona persona = personaRepository.findById(personaId)
        .orElseThrow(() -> new ResourceNotFoundException("Persona no encontrada :: " + personaId));
        
        persona.setEstatus(personaDetails.getEstatus());
        persona.setTelefono(personaDetails.getTelefono());
        persona.setSegundo_apellido(personaDetails.getSegundo_apellido());
        persona.setPrimer_apellido(personaDetails.getPrimer_apellido());
        persona.setNombre(personaDetails.getNombre());
        final Persona updatedPersona = personaRepository.save(persona);
        return ResponseEntity.ok(updatedPersona);
    }

    @DeleteMapping("/persona/{id}")
    public Map<String, Boolean> deletePersona(@PathVariable(value = "id") Long personaId)
         throws ResourceNotFoundException {
        Persona persona = personaRepository.findById(personaId)
       .orElseThrow(() -> new ResourceNotFoundException("Persona no encontrada :: " + personaId));

        personaRepository.delete(persona);
        Map<String, Boolean> response = new HashMap<>();
        response.put("eliminado", Boolean.TRUE);
        return response;
    }
}