package com.cqcias.springboot.evaluacion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "personas")

public class Persona {
	private long id;
    private String Nombre;
    private String Primer_apellido;
    private String Segundo_apellido;
    private String Telefono;
    private String Estatus;
 
    public Persona() {
  
    }
 
    public Persona(String nombre, String Primer_apellido, String Segundo_apellido, String Telefono, String Estatus) {
         this.Nombre = Nombre;
         this.Primer_apellido = Primer_apellido;
         this.Segundo_apellido = Segundo_apellido;
         this.Telefono = Telefono;
         this.Estatus = Estatus;
    }
 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
        public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
 
    @Column(name = "nombre", length = 100, nullable = false)
    public String getNombre() {
        return Nombre;
    }
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }
 
    @Column(name = "primer_apellido", length = 100, nullable = false)
    public String getPrimer_apellido() {
        return Primer_apellido;
    }
    public void setPrimer_apellido(String Primer_apellido) {
        this.Primer_apellido = Primer_apellido;
    }
 
    @Column(name = "segundo_apellido", length = 100, nullable = false)
    public String getSegundo_apellido() {
        return Segundo_apellido;
    }
    public void setSegundo_apellido(String Segundo_apellido) {
        this.Segundo_apellido = Segundo_apellido;
    }

    @Column(name = "telefono", length = 10, nullable = false)
    public String getTelefono() {
        return Telefono;
    }
    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }
    
    @Column(name = "estatus", length = 1, nullable = false)
    public String getEstatus() {
        return Estatus;
    }
    public void setEstatus(String Estatus) {
        this.Estatus = Estatus;
    }
    
    @Override
    public String toString() {
        return "Persona [id=" + id + ", nombre=" + Nombre + ", Primer_apellido=" + Primer_apellido + ", Segundo_apellido=" + Segundo_apellido
       + ", Telefono=" + Telefono + ", Estatus=" + Estatus + "]";
    }

}
